from .process import CronProcess
from .process import MetaMigrateValidateProcess
from .process import MetaEltValidateProcess
from .process import MetaFlowProcess
from .process import MetaEltValidateMetaDataProcess
from .process import MetaEltValidateMetaDataSuccessProcess
from .process import CronProcessList


class CronRequestHandler:
    @staticmethod
    def process():
        cron_process = CronProcess()
        return cron_process.process_cron()

    @staticmethod
    def process_get():
        cron_process = CronProcessList()
        return cron_process.response()

    @staticmethod
    def process_manual(data):
        cron_process = CronProcess()
        return cron_process.manual_etl(step=0, manual=data.manual)

    @staticmethod
    def reprocess_manual(data):
        cron_process = CronProcess()
        return cron_process.recalculate_etl(
            step=0,
            manual=data.manual,
            date=data.date_init
        )

    @staticmethod
    def consult_etl(task_id):
        etl = MetaMigrateValidateProcess(task_id)
        return etl.response()

    @staticmethod
    def consult_etl_complete(uuid):
        etl = MetaEltValidateProcess(uuid)
        return etl.response()

    @staticmethod
    def create_etl(data):
        etl = MetaFlowProcess(data)
        return etl.response()

    @staticmethod
    def consult_etl_flow(manual):
        etl = MetaEltValidateProcess(manual)
        return etl.response()

    @staticmethod
    def consult_meta_etl_flow(manual):
        etl = MetaEltValidateMetaDataProcess(manual)
        return etl.response()

    @staticmethod
    def consult_meta_etl_flow_success(manual):
        etl = MetaEltValidateMetaDataSuccessProcess(manual)
        return etl.response()

    @staticmethod
    def modify_meta_flow(data):
        etl = MetaFLowModify(
            data.manual,
            data.date_init
        )
        return etl.response()

    @staticmethod
    def consult_meta_erros():
        etl = MetaErrorsProcess()
        return etl.response()

    @staticmethod
    def charge_csv(name_path, csv_file):
        etl = ChargeCsvProcess(name_path, csv_file)
        return etl.process()

    @staticmethod
    def maintenance():
        return SourcesGlobals.delete_globals()
