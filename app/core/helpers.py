import os
import sys
import json
from datetime import datetime
from dateutil import relativedelta as rdelta
import dateutil.relativedelta
from app.core.constants import states
from app.core.models import MetaEtl, Dag, MetaFlow


def date_strptime(
        date: str = "2020-01-01",
        format_str: str = "%Y-%m-%d"
) -> (datetime, datetime):
    try:
        date = datetime.strptime(date, format_str)
    except Exception:
        date = datetime.strptime(date, "%Y-%m-%dT00:00:00")
    return date


def date_strftime(
        date,
        format_end: str = "%Y-%m-%d"
) -> str:
    date = datetime.strftime(date, format_end)
    return date


def change_month(
        month: str = "2020-01-01",
        retries: str = "0",
        status_prev: str = "SUCCESS"
) -> (datetime, datetime):
    d = date_strptime(month)
    if retries == "0" and status_prev == "SUCCESS":
        return d + dateutil.relativedelta.relativedelta(months=1), \
               d + dateutil.relativedelta.relativedelta(months=2)
    else:
        return d, d + dateutil.relativedelta.relativedelta(months=1)


def resolver_month(month: str):
    """

    :param month: mes de corte a ejecutar
    :return: mes de corte, mes siguiente
    """
    month = date_strptime(month)
    return month, month + dateutil.relativedelta.relativedelta(months=1)


def resolver_day(month: str):
    """

    :param month: mes de corte a ejecutar
    :return: mes de corte, mes siguiente
    """
    month = date_strptime(month)
    return month, month + dateutil.relativedelta.relativedelta(days=1)


def time_future(month: str) -> bool:
    month = date_strptime(month)
    today = datetime.now()
    if month <= today:
        return True
    else:
        return False


def dif_month(month: datetime, actual: datetime, period: str) -> bool:
    month = date_strptime(month)
    rd = rdelta.relativedelta(actual, month)
    if period == "month":
        if int("{0.months}".format(rd)) > 0:
            return True
        else:
            return False
    else:
        return True


def count_months(month: str) -> int:
    actual = datetime.now()
    month = date_strptime(month)
    rd = rdelta.relativedelta(actual, month)
    return rd.months


def change_folders3(ref: str, month) -> str:
    month_str = "0{}".format(month.month) if len(str(month.month)) == 1 else month.month
    day_str = "0{}".format(month.day) if len(
        str(month.day)
    ) == 1 else month.day
    return "{}/{}/{}/{}/{}/{}".format(
        os.environ.get('S3_NAME_KEY', 'components'),
        month.year,
        month_str,
        day_str,
        month.hour,
        ref
    )


def dict_meta(order_task: str) -> dict:
    dict_end = {}
    step = 0
    process_list = order_task.split("::")
    for process in process_list:
        dict_end["step_{}".format(step)] = {
            "components": "%s" % process,
            "status": "RECIEVED",
            "task_id": ""
        }
        step += 1
    return dict_end


def update_dict_meta(meta, step, task=None, status=None):
    if status is None:
        meta["step_%s" % step]["task_id"] = task
    else:
        meta["step_%s" % step]["status"] = status
    return meta


def last_etl_cut_run(uuid_ident: str, period: str, flag_success=False):
    meta = Dag.objects.filter(
        reference_uuid=uuid_ident,
        task_status="SUCCESS"
    ).order_by(
        '-date_cut',
        '-created_at'
    ).limit(1).first()
    meta_flow = MetaFlow.objects.filter(
        uuid=uuid_ident
    ).first()
    if meta is None:
        last_cut = date_strptime(
            str(meta_flow.date_init)
        )
        return last_cut
    else:
        last_cut = date_strptime(str(meta.date_cut))
    if flag_success:
        return last_cut
    else:
        if period == "month":
            return last_cut + dateutil.relativedelta.relativedelta(months=1)
        elif period == "day":
            return last_cut + dateutil.relativedelta.relativedelta(days=1)


def last_run(ident: str, meta_flow_uuid):
    meta = MetaEtl.objects.filter(
        etl_ident=ident
    ).order_by(
        '-date_cut',
        '-created_at'
    ).limit(1).first()
    meta_flow = MetaFlow.objects.filter(
        uuid=meta_flow_uuid
    ).first()
    if meta is None:
        return str(meta_flow.date_init), "PENDING"
    elif meta_flow.date_cut == "month":
        if meta.task_status == "SUCCESS":
            date_cut = meta.date_cut + dateutil.relativedelta.relativedelta(months=1)
            date_cut = date_strftime(date_cut)
            return str(date_cut), meta.task_status
        else:
            date_cut = date_strftime(meta.date_cut)
            return str(date_cut), meta.task_status
    elif meta_flow.date_cut == "day":
        if meta.task_status == "SUCCESS":
            date_cut = meta.date_cut + dateutil.relativedelta.relativedelta(days=1)
            date_cut = date_strftime(date_cut)
            return str(date_cut), meta.task_status
        else:
            date_cut = date_strftime(meta.date_cut)
            return str(date_cut), meta.task_status


def system_errors(self, exception, code):
    """

    :param self: self original donde fallo
    :param exception: excepción enviada
    :param code: codigo de error si existe
    :return:
    """
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    exception = str(exception).replace("'", "").replace(
        '"', '').replace(':', '').replace('{', '').replace(
        '}', '').replace('{', '').replace('(', '').replace(')', '').replace(r'\n', '')
    exc_type = str(exc_type).replace("'", "").replace(
        '"', '').replace(':', '').replace('{', '').replace(
        '}', '').replace('{', '').replace('(', '').replace(')', '')
    exception_dict = '{"retry": "%s", "error_code": "%s - %s",' \
        '"error_detail": "(%s) - %s - line: %s"}' % (
            self.retry, code, exception,
            exc_type, fname.replace("'", "").replace('"', ""), exc_tb.tb_lineno
        )
    save = {} if self._meta_mongo.exception == "" \
        else self._meta_mongo.exception
    save[self.retry] = json.loads(exception_dict, strict=False)
    self._meta_mongo.update(
        flag_exception=True,
        updated_at=datetime.now(),
        task_status=states.FAILURE,
        exception=save
    )


def month_prev(mont):
    return mont - dateutil.relativedelta.relativedelta(months=1)
