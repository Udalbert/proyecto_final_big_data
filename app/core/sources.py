import io
import os
import boto3
import pandas as pd
from pyspark.sql import SparkSession


def get_boto_instance_s3():
    if 's3' not in globals():
        globals()['s3'] = boto3.client(
            's3',
            aws_access_key_id=os.getenv(
                "AWS_ACCESS_KEY_ID"
            ),
            aws_secret_access_key=os.getenv(
                "AWS_SECRET_ACCESS_KEY"
            ),
        )
    return globals()['s3']


def get_boto_resource_s3():
    if 'resourceS3' not in globals():
        globals()['resourceS3'] = boto3.resource(
            's3',
            aws_access_key_id=os.getenv(
                "AWS_ACCESS_KEY_ID"
            ),
            aws_secret_access_key=os.getenv(
                "AWS_SECRET_ACCESS_KEY"
            ),
        )
    return globals()['resourceS3']


def read_s3_pandas(key: str, sep: str = ';', ext: str = 'csv'):
    s3 = get_boto_instance_s3()
    obj = s3.get_object(Bucket=os.environ.get('S3_BUCKET'), Key=key)
    if ext == 'csv':
        return pd.read_csv(
            io.BytesIO(obj['Body'].read()),
            error_bad_lines=False,
            sep=sep, dtype='unicode'
        )
    elif ext == 'parquet':
        return pd.read_parquet(
            io.BytesIO(obj['Body'].read())
        )


def upload_to_s3_csv(data, path):
    s3 = get_boto_resource_s3()
    s3.Bucket(
        os.environ.get('S3_BUCKET')
    ).put_object(
        Key=path,
        Body=data.to_csv(encoding='utf-8', sep=';', index=False)
    )


def upload_to_s3_parquet(data, path):
    """

    :param data: DataFrame
    :param path: ruta s3
            example:  "angel/demos/2021/03/prueba.parquet.snappy"
    :return:
    """
    s3 = get_boto_resource_s3()
    file = io.BytesIO()
    data.to_parquet(file)
    s3.Bucket(
        os.environ.get('S3_BUCKET')
    ).put_object(
        Key=path,
        Body=file
    )


class ManySparkContext:

    @staticmethod
    def context(name='default', master='spark-master'):
        spark = SparkSession.builder.master(
            master
        ).appName(
            name
        ).getOrCreate()
        return spark

    @staticmethod
    def context_s3(name='default', master='spark-master'):
        spark = SparkSession.builder.master(
            master
        ).appName(
            name
        ).config(
            'spark.jars.packages',
            'org.apache.hadoop:hadoop-aws:2.7.3'
        ).getOrCreate()
        spark._jsc.hadoopConfiguration().set(
            "fs.s3a.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem"
        )
        spark._jsc.hadoopConfiguration().set(
            "fs.s3a.awsAccessKeyId",
            os.environ.get('AWS_ACCESS_KEY_ID')
        )
        spark._jsc.hadoopConfiguration().set(
            "fs.s3a.awsSecretAccessKey",
            os.environ.get('AWS_SECRET_ACCESS_KEY')
        )
        return spark

    @staticmethod
    def context_mongo(name='default', master='spark-master'):
        spark = SparkSession.builder.config(
            master, name
        ).config(
            'spark.jars.packages',
            'org.mongodb.spark:mongo-spark-connector_2.11:2.4.1'
        ).config(
            "spark.mongodb.input.uri", "mongodb://mongo/mongoerp"
        ).config(
            "spark.mongodb.output.uri", "mongodb://mongo/mongoerp"
        ).getOrCreate()
        return spark




