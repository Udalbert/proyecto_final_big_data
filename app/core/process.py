import json
import pandas as pd
from app.core.models import MetaEtl, MetaFlow, Dag
from app.core.helpers import dict_meta, last_etl_cut_run,\
    update_dict_meta
from app.core.constants import TAKS, ETL, ELT_IDENT_CONF, \
    response_exist
from app.core.queryset import QuerySet
from app.core.sources import upload_to_s3_csv
from datetime import datetime
from app.core.helpers import resolver_month, update_dict_meta,\
    dif_month, resolver_day, time_future, date_strftime, date_strptime
from app.core.helpers import last_run, count_months
from config.settings import connect_db, close_db
from .queryset import QuerySet
from .components.transform import spark_DS
from .components.extract import desembolsos_producto
from .components.extract import desembolsos_recargas


class CronProcess:
    """
    Execute Dags
    """

    def process_cron(
            self,
            step=0
    ):
        flow = MetaFlow.objects.filter(
            activate=True
        )
        for elt_run in flow:
            etl_uuid = elt_run.uuid
            elt_list = str(elt_run.reference).lower()
            process_list = elt_list.split("::")
            etl = Dag.objects.all().order_by(
                "-created_at"
            ).limit(1).first()

            meta_etl = self.create_dag(
                etl,
                etl_uuid,
                elt_run,
                process_list
            )

        return "RUNN ALL"

    def process_step(
            self,
            step=0,
            uuid=None,
            recalculate=False
    ):
        meta_etl = Dag.objects.get(
            uuid=uuid,
        )
        if step < meta_etl.count_step:
            process_list = []
            for steps in range(0, meta_etl.count_step):
                process_list.append(meta_etl.elt_meta["step_%s" % steps]["components"])

        else:
            count = 0
            for step in range(0, meta_etl.count_step):
                count = count + 1 if meta_etl.elt_meta["step_%s" % step][
                                         "status"
                                     ] == "SUCCESS" else count
            status_etl = "SUCCESS" \
                if meta_etl.count_step == count else "FAILURE"
            meta_etl.update(
                updated_at=datetime.now(),
                task_status=status_etl
            )
        return "run step {}".format(step)

    def recalculate_etl(self, step=0, manual=1, date="2020-01-01"):
        meta = MetaFlow.objects.filter(manual=manual).first()
        etl_uuid = meta.uuid
        process_list = str(meta.reference).split("::")
        meta_etl = QuerySet.create_dag(
            reference_uuid=etl_uuid,
            date_cut=date,
            count_step=meta.count_step,
            elt_meta=dict_meta(str(meta.reference))
        )

        return str(meta_etl.uuid)

    def manual_etl(self, step=0, manual=1):
        meta_flow = MetaFlow.objects.filter(
            manual=manual
        ).first()
        etl_uuid = meta_flow.uuid
        elt_list = str(meta_flow.reference)
        process_list = elt_list.split("::")
        etl = Dag.objects.filter(
            reference_uuid=etl_uuid
        ).order_by(
            '-date_cut',
            "-created_at"
        ).limit(1).first()

        meta_etl = self.create_dag(
            etl,
            etl_uuid,
            meta_flow,
            process_list
        )

        return str(meta_etl.uuid)

    @staticmethod
    def create_dag(
            etl,
            etl_uuid,
            elt_run,
            process_list
    ):

        if etl is not None and etl.task_status == "SUCCESS":
            date_cut = last_etl_cut_run(
                etl_uuid,
                elt_run.date_cut,
                flag_success=True
            )
        else:
            date_cut = last_etl_cut_run(
                etl_uuid,
                elt_run.date_cut
            )
        meta_etl = QuerySet.create_dag(
            reference_uuid=etl_uuid,
            date_cut=date_cut,
            count_step=len(process_list),
            elt_meta=dict_meta(str(elt_run.reference))
        )
        return meta_etl


class MetaMigrateValidateProcess:
    def __init__(self, task_id: str):
        self.task_id = task_id

    @property
    def model(self):
        return MetaEtl.objects.filter(
            task_id=self.task_id
        ).first()

    def response(self):
        if self.model is not None:
            return self.model.as_dict()
        else:
            return {
                "task_id": self.task_id,
                "error": response_exist
            }


class MetaEltValidateProcess:
    def __init__(self, manual: str):
        self.manual = manual

    @property
    def model(self):
        flow = MetaFlow.objects.filter(
            manual=self.manual
        ).first()
        dags = Dag.objects.filter(
            reference_uuid=flow.uuid
        ).order_by(
            '-created_at'
        ).limit(30)
        response = {}
        index = 0
        for dag in dags:
            response[index] = {}
            response[index][
                "reference_uuid"
            ] = dag.reference_uuid
            response[index][
                "uuid"
            ] = dag.uuid
            response[index][
                "task_status"
            ] = dag.task_status
            response[index][
                "elt_meta"
            ] = dag.elt_meta
            response[index][
                "date_cut"
            ] = dag.date_cut
            response[index][
                "created_at"
            ] = dag.created_at
            index += 1
        return response

    def response(self):
        if self.model is not None:
            return self.model
        else:
            return {
                "id": self.manual,
                "error": response_exist
            }


class MetaEltValidateMetaDataProcess:
    def __init__(self, manual: str):
        self.manual = manual

    @property
    def model(self):
        flow = MetaFlow.objects.filter(
            manual=self.manual
        ).first()
        dags = Dag.objects.filter(
            reference_uuid=flow.uuid,
            task_status__in=("PENDING", "RECEIVED", "FAILURE")
        ).order_by(
            '-created_at'
        ).limit(20)
        response = {}
        index = 0
        for dag in dags:
            meta_etl = MetaEtl.objects.filter(
                reference_uuid=dag.uuid
            ).order_by(
                '-created_at'
            ).first()
            response[index] = {}
            response[index][
                "task_status"
            ] = meta_etl.task_status
            response[index][
                "reference"
            ] = flow.reference
            response[index][
                "task_id"
            ] = meta_etl.task_id
            response[index][
                "exception"
            ] = meta_etl.exception
            response[index][
                "etl_ident"
            ] = meta_etl.etl_ident
            response[index][
                "date_cut"
            ] = meta_etl.date_cut
            response[index][
                "retry"
            ] = meta_etl.retry
            response[index][
                "time"
            ] = meta_etl.time
            response[index][
                "task_meta"
            ] = meta_etl.task_meta
            index += 1
        return response

    def response(self):
        if self.model is not None:
            return self.model
        else:
            return {
                "id": self.manual,
                "error": response_exist
            }


class MetaEltValidateMetaDataSuccessProcess:
    def __init__(self, manual: str):
        self.manual = manual

    @property
    def model(self):
        flow = MetaFlow.objects.filter(
            manual=self.manual
        ).first()
        dags = Dag.objects.filter(
            reference_uuid=flow.uuid,
            task_status__in=("SUCCESS",)
        ).order_by(
            '-created_at'
        ).limit(40)
        response = {}
        index = 0
        for dag in dags:
            meta_etl = MetaEtl.objects.filter(
                reference_uuid=dag.uuid
            ).order_by(
                '-created_at'
            ).first()
            response[index] = {}
            response[index][
                "task_status"
            ] = meta_etl.task_status
            response[index][
                "reference"
            ] = flow.reference
            response[index][
                "task_id"
            ] = meta_etl.task_id
            response[index][
                "dir_meta"
            ] = meta_etl.dir_meta
            response[index][
                "exception"
            ] = meta_etl.exception
            response[index][
                "etl_ident"
            ] = meta_etl.etl_ident
            response[index][
                "date_cut"
            ] = meta_etl.date_cut
            response[index][
                "retry"
            ] = meta_etl.retry
            response[index][
                "time"
            ] = meta_etl.time
            response[index][
                "task_meta"
            ] = meta_etl.task_meta
            index += 1
        return response

    def response(self):
        if self.model is not None:
            return self.model
        else:
            return {
                "id": self.manual,
                "error": response_exist
            }


class CronProcessList:

    @property
    def model(self):
        meta_flow = MetaFlow.objects.all()
        response = {}
        index = 0
        for process in meta_flow:
            reference = Dag.objects.filter(
                reference_uuid=process.uuid
            ).order_by(
                '-created_at'
            ).first()
            if reference is not None:
                response[index] = {}
                response[index][
                    "created_at"
                ] = reference.created_at
                response[index][
                    "date_cut"
                ] = reference.date_cut
                response[index][
                    "manual"
                ] = process.manual
                response[index][
                    "reference"
                ] = process.reference
                response[index][
                    "elt_meta"
                ] = reference.elt_meta
                response[index][
                    "task_status"
                ] = reference.task_status
                index += 1

        return response

    def response(self):
        if self.model is not None:
            return self.model
        else:
            return response_exist


class MetaFlowProcess:
    def __init__(self, data):
        self.data = data

    @property
    def model(self):
        validate = MetaFlow.objects.filter(
            reference=self.data.reference
        ).first()
        if validate is None:
            MetaFlow.objects.create(
                reference=self.data.reference,
                manual=self.manual(),
                count_step=self.data.count_step,
                date_cut=self.data.date_cut,
                date_init=self.data.date_init
            )
            return {"created": str(self.data.reference)}
        else:
            return {"not_created": "reference exist"}

    @staticmethod
    def manual():
        metaflow = MetaFlow.objects.all()
        return len(metaflow) + 1

    def response(self):
        return str(self.model)


class MetaFlowValidateProcess:
    def __init__(self, uuid: str):
        self.uuid = uuid

    @property
    def model(self):
        return MetaFlow.objects.filter(
            uuid=self.uuid
        ).first()

    def response(self):
        if self.model is not None:
            return self.model.as_dict()
        else:
            return {
                "uuid": self.uuid,
                "error": response_exist
            }


class TaskProcess:

    def __init__(self,
                 request,
                 reference_uuid,
                 date_cut,
                 meta_flow_uuid,
                 step,
                 ident: str,
                 recalculate: bool = False
                 ):
        self.request_task = request
        self.reference_uuid = reference_uuid
        self.date_cut = date_cut
        self.meta_flow_uuid = meta_flow_uuid
        self.step = step
        self.ident = ident
        self.recalculate = recalculate

    @property
    def etl_module(self):
        return ELT_IDENT_CONF[int(self.ident) - 1][1]

    @property
    def dates(self):
        meta_flow = MetaFlow.objects.filter(
            uuid=self.meta_flow_uuid
        ).first()
        if meta_flow.date_cut == 'month':
            date_init, date_end = resolver_month(
                month=self.date_cut
            )
        elif meta_flow.date_cut == 'day':
            date_init, date_end = resolver_day(
                month=self.date_cut
            )
        return date_init, date_end, meta_flow.date_cut

    def exec_etl(self, last_cut):
        date_init, date_end, period = self.dates
        if str(self.request_task.request.retries) == "0" and \
                dif_month(last_cut, datetime.now(), period):
            MetaEtl.objects.create(
                task_id=str(self.request_task.request.id),
                task_meta=self.request_task.request.__dict__,
                etl_ident=self.ident,
                last_run=datetime.now().strftime("%Y-%m-%d"),
                retry=str(self.request_task.request.retries),
                date_cut=date_init,
                reference_uuid=self.reference_uuid,
            )

        process = desembolsos_producto.Etl(
            date_init,
            date_end,
            self.meta_flow_uuid,
            str(self.request_task.request.retries),
            str(self.request_task.request.id)
        )
        valid = process.process()
        if not valid and self.request_task.request.retries <= 2:
            close_db()
            self.request_task.retry(
                countdown=2 ** self.request_task.request.retries
            )
        if not valid and self.request_task.request.retries > 2:
            self.status_task = "FAILURE"
        if valid:
            self.status_task = "SUCCESS"
        return True

    def exec_task(self):
        """
        last_cut: fecha de ultimo corte en que funciono la etl
        date_cut: fecha del corte en que se ejecuta el dag
        :return:
        """
        connect_db()
        last_cut, status = last_run(self.ident, self.meta_flow_uuid)
        if time_future(str(self.date_cut)):
            if str(last_cut) <= str(self.date_cut) or self.recalculate:
                #  INGRESA CUANDO LA ETL Y DAG PARA ESE
                #  CORTE NO SE HAN EJECUTADO
                self.exec_etl(last_cut)
            elif status != "SUCCESS":
                #  INGRESA CUANDO EXISTE UNA ETL
                #  YA EJECUTADA EN ESE CORTE Y AUN NO EL DAG
                #  Y FUE FALLIDA
                self.exec_etl(last_cut)
            elif status == "SUCCESS" and str(last_cut) > str(self.date_cut):
                self.status_task = status
            else:
                #SE DEBE GENERAR ALERTA PORQUE UNA ETL DEL DAG
                #ESTA ATRASADA O ADELANTADA VARIOS MESES
                self.status_task = "FAILURE"
            meta = Dag.objects.get(uuid=self.reference_uuid)
            meta.update(
                elt_meta=update_dict_meta(
                    meta.elt_meta,
                    self.step,
                    status=self.status_task
                )
            )
            # pasa al siguiente paso del dag
            cron_process = CronProcess()
            cron_process.process_step(
                step=self.step + 1,
                uuid=self.reference_uuid,
                recalculate=self.recalculate
            )
            if self.request_task.request.retries > 2:
                close_db()
            return True
        else:
            close_db()
            return False
