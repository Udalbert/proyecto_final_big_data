from app.core.sources import ManySparkContext
from pyspark.sql import functions as F
from pyspark.sql.functions import udf
from pyspark.sql.types import IntegerType
from pyspark.sql.types import StringType


def spark_transform():
    spark = ManySparkContext.context_s3(
        name="DS"
    )
    df = spark.read.option("delimiter", ";").csv(
        "s3a://zinobe-test/angel/demos/etl/2021/04/modular/disbursements.csv",
        header=True
    )

    df.show()

    df_1 = df.groupBy(
        "numero_de_identificacion"
    ).agg(
        F.count("monto_prestamo").alias("conteo"),
        F.sum("monto_prestamo").alias("monto_prestamo"),
        F.first("fecha_de_transferencia").alias("fecha_de_transferencia_inicial"),
        F.last("fecha_de_transferencia").alias("fecha_de_transferencia_final"),
        F.last("tipo_identificion").alias("tipo_identificion"),
        F.last("nombre").alias("nombre"),
        F.sum("fga").alias("fga"),
        F.sum("valor_desembolsado").alias("valor_desembolsado"),
    )

    @udf(StringType())
    def promedio(*args):
        conteo = 0 if args[0] is None else float(args[0])
        valor = 0 if args[1] is None else float(args[1])
        return valor / conteo

    df_1 = df_1.withColumn(
        "promedio_fga",
        promedio(
            df_1["conteo"],
            df_1["fga"]
        )
    )
    df_1 = df_1.withColumn(
        "promedio_desembolso",
        promedio(
            df_1["conteo"],
            df_1["valor_desembolsado"]
        )
    )

    df_1 = df_1.select(
        'numero_de_identificacion',
        'fecha_de_transferencia_inicial',
        'fecha_de_transferencia_final',
        df_1.monto_prestamo.cast(IntegerType()).alias('monto_prestamo'),
        df_1.conteo.cast(IntegerType()).alias('conteo'),
        'tipo_identificion',
        'nombre',
        df_1.fga.cast(IntegerType()).alias('fga'),
        df_1.valor_desembolsado.cast(IntegerType()).alias('valor_desembolsado'),
        df_1.promedio_fga.cast(IntegerType()).alias('promedio_fga'),
        df_1.promedio_fga.cast(IntegerType()).alias('promedio_desembolso'),
    )

    df_1 = df_1.filter(
        df_1["conteo"] >= 2
    )

    df_1.repartition(1).write.format('csv').option('header', True).option(
        "delimiter",
        ";"
    ).mode('overwrite').save("../../temporal/ds_validate")
    return True
