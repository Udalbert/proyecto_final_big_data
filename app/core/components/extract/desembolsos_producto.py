import os
import numpy as np
import pandas as pd
from pandas.io import sql
import time
from datetime import datetime
from sqlalchemy.engine.base import Connection
from botocore.exceptions import ClientError
from .constants import QUERY_DISBURSEMENTS
from app.core.constants import states
from config.settings import engine_corebanking, LIMIT_CONSULT, \
    engine_datamart, INSERT_DB
from app.core.models import MetaEtl
from app.core.helpers import change_folders3, system_errors
from .helpers import comision
from app.core.sources import upload_to_s3_csv


class Etl(object):

    def __init__(self, date_init, date_end, meta_flow_uuid, retry, task_id):
        self.df = pd.DataFrame()
        self.engine = engine_corebanking()
        self.engine_datamart = engine_datamart()
        self.date_init = date_init
        self.date_end = date_end
        self.meta_flow_uuid = meta_flow_uuid
        self.retry = retry
        self._limit = LIMIT_CONSULT
        self._meta_mongo = MetaEtl.objects.get(task_id=task_id)

    def process(self) -> bool:
        flag_transform = False
        flag_load = False
        self.time_init = time.time()
        if self.retry == "0":
            self._meta_mongo.update(
                task_status=states.RECEIVED
            )
        else:
            self._meta_mongo.update(
                task_status=states.RETRY,
                retry=self.retry
            )
        flag_extract = self.extract()
        if flag_extract:
            flag_transform = self.transform()
            if flag_transform:
                flag_load = self.load()
        if flag_extract and flag_transform and flag_load:
            self.time_end = time.time()
            time_total = self.time_end - self.time_init
            self._meta_mongo.update(
                task_status=states.SUCCESS,
                time=time_total
            )
            return True
        else:
            return False

    def extract(self) -> bool:
        try:
            self.df_ds = pd.read_sql(
                QUERY_DISBURSEMENTS.format(
                    date_init=self.date_init,
                    date_end=self.date_end,
                    limit=self._limit
                ),
                self.engine
            )
            return True
        except Exception as exception:
            system_errors(
                self,
                "{} {} {}".format(
                    str(exception),
                    str(os.environ.get('S3_BUCKET')),
                    change_folders3(
                        ref="modular/contacts.csv",
                        month=self.date_end
                    )
                ),
                2100
            )
            return False
        finally:
            if isinstance(self.engine, Connection):
                self.engine.close()

    def transform(self) -> bool:
        try:
            self.df_ds["name_product"] = "Lineru Tradicional"
            self.df_ds["company"] = "Lineru"
            self.df_ds[
                "comisiones"
            ] = self.df_ds["items"]
            self.df_ds["credit_line_id"] = 0
            self.df_ds["fga"] = 0
            self.df_ds["uid"] = self.df_ds[
                "uid"
            ].astype(str)

            self.df_ds = self.df_ds[[
                "confirm_date", "uid", "tipo_identifición",
                "cedula", "nombre", "amount", "fga", "name",
                "bat_name", "bank_account_num",
                "company", "name_product", "comisiones"
            ]]
            select_union = [
                "fecha_desembolso", "uid", "tipo_identifición",
                "cedula", "nombre", "principal", "fga", "bank_name",
                "account_type", "account_num",
                "company", "name_product", "comisiones"
            ]
            self.df_ds.columns = select_union
            self.df_ds['fecha_desembolso'] = self.df_ds[
                'fecha_desembolso'
            ].astype(str)

            self.df_ds["SURA"] = self.df_ds.apply(
                comision,
                args=("SURA",),
                axis=1
            )
            self.df_ds["fng"] = self.df_ds.apply(
                comision,
                args=("fng",),
                axis=1
            )
            self.df_ds["monto_prestamo"] = self.df_ds["principal"]
            self.df_ds[
                "valor_desembolsado"
            ] = self.df_ds["monto_prestamo"] - \
                self.df_ds["SURA"]
            self.df_ds = self.df_ds[[
                "fecha_desembolso", "uid", "tipo_identifición",
                "cedula", "nombre", "principal",
                "valor_desembolsado",
                "fga", "bank_name", "bank_name", "account_type",
                "company", "name_product", "SURA", "fng"
            ]]
            self.df_ds.columns = [
                "fecha_de_transferencia", "no_aplicacion",
                "tipo_identificion", "numero_de_identificacion",
                "nombre", "monto_prestamo",
                "valor_desembolsado", "fga", "banco_de_la_aplicacion",
                "banco_usado", "cuenta", "producto", "subproducto", "sura", "fng"
            ]

            self.df_ds["valor_desembolsado"] = self.df_ds[
                "valor_desembolsado"
            ].fillna(0).astype(np.int64)
            return True
        except Exception as exception:
            system_errors(
                self,
                exception,
                2200
            )
            return False

    def load(self) -> bool:
        try:
            #  comentado para no duplicar data en datamart existente
            # sql.to_sql(
            #     self.df_end,
            #     name='modular_disbursements',
            #     if_exists=INSERT_DB,
            #     con=self.engine_datamart,
            #     index=False
            # )
            folder_original = change_folders3(
                ref="modular/disbursements.csv",
                month=self.date_end
            )
            upload_to_s3_csv(
                self.df_ds,
                folder_original
            )
            self._meta_mongo.update(
                updated_at=datetime.now(),
                dir_meta=folder_original
            )
            return True
        except Exception as exception:
            system_errors(
                self,
                "{}".format(
                    str(exception)
                ),
                2300
            )
            return False
        finally:
            if isinstance(self.engine_datamart, Connection):
                self.engine_datamart.close()
