import json
import numpy as np
from datetime import datetime


def comision(df, val):
    try:
        response = 0
        result = json.loads(
            str(
                df["comisiones"]
            ).replace(
                "'",
                '"'
            ).replace("True", 'true').replace("False", 'false'))
        for resp in result["prepaid"]:
            if str(resp["name"]) == val:
                response = resp["amount"]
        return response
    except Exception:
        return 0


def fecha_formato_banks(fecha):
    return datetime.strptime(f"{fecha[:2]}/{fecha[3:]}", "%m/%Y").strftime("%Y-%m-%d")


def objeto_final_banks(df):
    list_balance = [(
        df["rfc"],
        df["month_year"][index],
        fecha_formato_banks(str(df["month_year"][index])),
        df["inflow"][index],
        df["outflow"][index],
        df["initial_balance"][index],
        df["final_balance"][index],
        df["avg_balance"][index],
        0
    ) for index in range(0, len(df["month_year"]))]
    dtype = [
        ('rfc', 'U40'), ('month_year', 'U30'), ('date_format', 'U40'),
        ('inflow', float), ('outflow', float), ('initial_balance', float), ('final_balance', float),
        ('avg_balance', float), ('order', float)
    ]
    list_balance = np.array(
        list_balance,
        dtype=dtype
    )
    revision = np.sort(
        list_balance,
        order='date_format'
    )
    revision = [[
        revision[index][0],
        revision[index][1],
        revision[index][3],
        revision[index][4],
        revision[index][5],
        revision[index][6],
        revision[index][7],
        index
    ] for index in range(0, revision.size)]
    avg_inflow = np.average(
        [value[3] for value in revision]
    ) * len(revision)
    avg_outflow = np.average(
        [value[4] for value in revision]
    ) * len(revision)
    objeto_final = [
        {
            "month_year":value[1], "inflow": value[2],
            "outflow": value[3],
            "initial_balance": value[4], "final_balance": value[5],
            "avg_balance": value[6], "order": value[7]
        }
        for value in revision
    ]
    return objeto_final, avg_inflow, avg_outflow

def descomponer_response_banks(df):
    response = str(df["data"]).replace("'",'"').replace("True",'true').replace("False",'false').replace("None",'"None"')
    response = json.loads(response)
    new_response = []
    rfc = df["identification"]
    bank_name = response["bank_name"]
    month_year = response["month_year"]
    initial_balance = response["initial_balance"]
    inflow = response["inflow"]
    outflow = response["outflow"]
    final_balance = response["final_balance"]
    avg_balance = response["avg_balance"]
    new_response.append([
        rfc, bank_name, month_year, initial_balance, inflow, outflow, final_balance, avg_balance
    ])

    return new_response
