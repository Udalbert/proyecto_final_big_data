QUERY_DISBURSEMENTS: str = """
SELECT 
    app.confirm_date,
    app.uid,
    app.amount,
    b.name,
    bat.name as bat_name,
    app.bank_account_num,
    pre.items
FROM
    applications AS app
        LEFT JOIN
    banks AS b ON app.id_bank = b.id
        LEFT JOIN
    banks_account_types AS bat ON app.id_bank_account_type = bat.id
        LEFT JOIN
    prepaids AS pre ON pre.application_id = app.id
WHERE
    app.confirm_date IS NOT NULL
    AND app.created_at >= '{date_init}' AND app.created_at < '{date_end}'
LIMIT {limit}
;
"""