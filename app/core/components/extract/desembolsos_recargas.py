import os
import json
import pandas as pd
from pandas.io import sql
import logging
import time
from datetime import datetime
from sqlalchemy.engine.base import Connection
from app.core.constants import states
from config.settings import engine_datamart
from config.settings import start_connection_workspace, \
    NAME_DB_WORKSPACE
from app.core.models import MetaEtl
from app.core.helpers import system_errors, change_folders3
from app.core.sources import upload_to_s3_parquet
from .helpers import objeto_final_banks as objeto_final
from .helpers import descomponer_response_banks as descomponer_response


class Etl(object):

    def __init__(self, rfc, date_init, date_end, meta_flow_uuid, retry, task_id):
        self.df = pd.DataFrame()
        self.engine_datamart = engine_datamart()
        self.rfc = rfc
        self.date_init = date_init
        self.date_end = date_end
        self.date_end = datetime.now()
        self.meta_flow_uuid = meta_flow_uuid
        self.retry = retry
        self._meta_mongo = MetaEtl.objects.get(task_id=task_id)

    def process(self) -> bool:
        flag_transform = False
        flag_load = False
        self.time_init = time.time()
        if self.retry == "0":
            self._meta_mongo.update(
                task_status=states.RECEIVED
            )
        else:
            self._meta_mongo.update(
                task_status=states.RETRY,
                retry=self.retry
            )
        flag_extract = self.extract()
        if flag_extract:
            flag_transform = self.transform()
            if flag_transform:
                flag_load = self.load()
        if flag_extract and flag_transform and flag_load:
            self.time_end = time.time()
            time_total = self.time_end - self.time_init
            self._meta_mongo.update(
                task_status=states.SUCCESS,
                time=time_total
            )
            return True
        else:
            return False

    def extract(self) -> bool:
        try:
            connection = start_connection_workspace()
            workspace_dict_data_document = connection[
                NAME_DB_WORKSPACE
            ]["data_document"]

            self.df_banks = pd.DataFrame.from_dict(
                workspace_dict_data_document.find({
                    "identification": self.rfc,
                    "created_at": {
                        "$gt": self.date_init,
                        "$lt": self.date_end
                    }
                })
            )

            return True
        except Exception as exception:
            system_errors(
                self,
                f"{exception}, name_DB: {NAME_DB_WORKSPACE}",
                2100
            )
            return False

    def transform(self) -> bool:
        try:
            self.df_banks["provisional"] = self.df_banks.apply(descomponer_response, axis=1)
            df_banks_end = self.df_banks["provisional"]
            banks_list = []
            for value in range(0, len(df_banks_end.axes[0])):
                for first in range(0, len(df_banks_end[value])):
                    banks_list.append(df_banks_end[value][first])

            df_final = pd.DataFrame(
                banks_list,
                columns=[
                    "rfc", "bank_name", "month_year",
                    "initial_balance", "inflow", "outflow",
                    "final_balance", "avg_balance"
                ]
            )

            df_final = df_final.groupby(
                ["rfc", "month_year"],
                as_index=False
            ).agg(
                {
                    "inflow": "sum",
                    "outflow": "sum",
                    "initial_balance": "sum",
                    "final_balance": "sum",
                    "avg_balance": "sum"
                }
            )

            df_final_group = df_final.groupby(
                ["rfc"], as_index=False
            ).agg(
                {
                    "month_year": lambda x: list(x),
                    "inflow": lambda x: list(x),
                    "outflow": lambda x: list(x),
                    "initial_balance": lambda x: list(x),
                    "final_balance": lambda x: list(x),
                    "avg_balance": lambda x: list(x)
                }
            )
            df_final_group[
                "transactions_by_month_year"
            ], df_final_group[
                "avg_inflow"
            ], df_final_group[
                "avg_outflow"
            ] = zip(*df_final_group.apply(objeto_final, axis=1))
            self.df_end = df_final_group[[
                "rfc", "transactions_by_month_year",
                "avg_inflow", "avg_outflow"
            ]]
            return True
        except Exception as exception:
            system_errors(
                self,
                exception,
                2200
            )
            return False

    def load(self) -> bool:
        try:
            folder_original = change_folders3(
                ref="desembolsos_recargas/sat.parquet.snappy",
                month=self.date_end
            )
            upload_to_s3_parquet(
                self.df_end,
                folder_original
            )
            self._meta_mongo.update(
                updated_at=datetime.now(),
                dir_meta=folder_original
            )
            return True
        except Exception as exception:
            system_errors(
                self,
                f"{str(exception)}",
                2300
            )
            return False
        finally:
            if isinstance(self.engine_datamart, Connection):
                self.engine_datamart.close()
