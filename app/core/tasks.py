from celery import shared_task
from app.core.process import TaskProcess


@shared_task(bind=True)
def contabilidad_lineru_movimiento_mensual(
        self,
        reference_uuid: str,
        date_cut: str,
        meta_flow_uuid: str,
        step: int,
        recalculate=False
):
    process = TaskProcess(
        self,
        reference_uuid,
        date_cut,
        meta_flow_uuid,
        step,
        "1",
        recalculate=recalculate
    )
    process.exec_task()
    return True
