from app.core.models import MetaEtl, Dag, \
    MetaFlow


class QuerySet:

    @staticmethod
    def create_dag(**kwargs):
        dag = Dag.objects.create(
            **kwargs
        )
        return dag

    @staticmethod
    def update_dag(uuid, **kwargs):
        meta_etl = Dag.objects.filter(
            uuid=uuid
        ).first()
        meta_etl.update(
            **kwargs
        )
        return True

    @staticmethod
    def create_meta_flow(**kwargs):
        return MetaFlow.objects.create(
            **kwargs
        )

    @staticmethod
    def find_meta_flow(**kwargs):
        return MetaFlow.objects.filter(
            **kwargs
        )

    @staticmethod
    def find_meta_flow_order(order, **kwargs):
        return MetaFlow.objects.filter(
            **kwargs
        ).order_by(
            order
        )

    @staticmethod
    def find_meta_flow_one(order, **kwargs):
        return MetaFlow.objects.filter(
            **kwargs
        ).order_by(
            order
        ).first()

    @staticmethod
    def create_meta_etl(**kwargs):
        dag = MetaEtl.objects.create(
            **kwargs
        )
        return dag

    @staticmethod
    def update_meta_etl(uuid, **kwargs):
        meta_etl = MetaEtl.objects.filter(
            uuid=uuid
        ).first()
        meta_etl.update(
            **kwargs
        )
        return True

