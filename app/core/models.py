import os
import uuid
from datetime import datetime
from littlenv import littlenv
from celery import states
from mongoengine import DynamicDocument
from mongoengine import fields as models
from .constants import ELT_IDENT
from .constants import TASK_STATE_CHOICES

littlenv.load()


class MetaEtl(DynamicDocument):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid.uuid4,
        editable=False
    )
    task_status = models.StringField(
        max_length=50,
        default=states.PENDING,
        db_index=True,
        choices=TASK_STATE_CHOICES
    )
    task_id = models.StringField(
        max_length=255,
        unique=True,
        db_index=True
    )
    task_meta = models.DictField()
    flag_exception = models.BooleanField(
        default=False
    )
    exception = models.DictField()
    etl_ident = models.StringField(
        choices=ELT_IDENT,
        max_length=3,
        default=''
    )
    date_cut = models.DateField(
        default='',
    )
    dir_meta = models.StringField(
        default=''
    )
    last_run = models.DateField(
        default='',
    )
    retry = models.StringField(
        max_length=3,
        default=''
    )
    time = models.IntField(
        default=0
    )
    reference_uuid = models.UUIDField(
        default='',
        editable=False
    )
    created_at = models.DateTimeField(
        default=datetime.now,
    )
    updated_at = models.DateTimeField(
        default=datetime.now,
        null=True
    )

    meta = {'db_alias': os.getenv("MONGO_NAME")}

    def as_dict(self):
        return {
            'task_id': self.task_id,
            'uuid': str(self.uuid),
            'task_status': self.task_status,
            'flag_exception': self.flag_exception,
            'etl_ident': self.etl_ident,
            'last_run': self.last_run,
            'retry': self.retry,
            'time': self.time,
            'created_at': self.created_at,
            'updated_at': self.updated_at
        }


class Dag(DynamicDocument):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid.uuid4,
        editable=False
    )
    reference_uuid = models.UUIDField(
        default='',
        editable=False
    )
    date_cut = models.DateField(
        default='',
    )
    elt_meta = models.DictField()
    task_status = models.StringField(
        max_length=50,
        default=states.PENDING,
        db_index=True,
        choices=TASK_STATE_CHOICES
    )
    created_at = models.DateTimeField(
        default=datetime.now,
    )
    updated_at = models.DateTimeField(
        default=datetime.now,
        null=True
    )
    deleted_at = models.DateTimeField(
        null=True
    )

    meta = {'db_alias': os.getenv("MONGO_NAME")}

    def as_dict(self):
        return {
            'reference_uuid': str(self.reference_uuid),
            'uuid': str(self.uuid),
            'task_status': self.task_status,
            'elt_meta': self.elt_meta,
            'date_cut': self.date_cut,
            'created_at': self.created_at,
            'updated_at': self.updated_at
        }


class MetaFlow(DynamicDocument):
    uuid = models.UUIDField(
        db_index=True,
        default=uuid.uuid4,
        editable=False
    )
    reference = models.StringField(
        default=''
    )
    manual = models.IntField(
        db_index=True,
        unique=True,
    )
    count_step = models.IntField(
        default=1
    )
    activate = models.BooleanField(
        default=True
    )
    date_cut = models.StringField(
        default='month'
    )
    date_init = models.StringField(
        default='2020-01-01'
    )
    created_at = models.DateTimeField(
        default=datetime.now,
    )
    updated_at = models.DateTimeField(
        default=datetime.now,
        null=True
    )
    deleted_at = models.DateTimeField(
        null=True
    )

    meta = {'db_alias': os.getenv("MONGO_NAME")}
