from celery import states


ELT_IDENT: tuple = (
    ('1', 'desembolsos_producto'),
    ('2', 'desembolso_recargas'),
    ('3', 'spark_DS'),
)


ALL_STATES = sorted(states.ALL_STATES)
TASK_STATE_CHOICES = sorted(zip(ALL_STATES, ALL_STATES))

TAKS = "app.core.tasks"
ETL = "app.core.components"
response_exist = {"error": "not exist"}
