from fastapi import APIRouter
from starlette import status
from starlette.responses import JSONResponse
from config.settings import API_VERSION
router = APIRouter()


@router.get("/", tags=["meta"])
async def root():
    return {"MicroService": "Spark-FLow by ZiBeports is Ready"}


@router.get("/version", tags=["meta"])
async def version():
    response = {
        "version": API_VERSION,
        "message": "SparkFLow by ZiBeports"
    }
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=response
    )


@router.get("/health", tags=["status"])
async def health_check():
    response = {"status": "ok"}
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=response
    )
