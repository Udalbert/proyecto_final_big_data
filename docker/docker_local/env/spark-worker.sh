#Environment variables used by the spark workers
#Do not touch this unless you modify the compose master
SPARK_MASTER=spark://spark-master:7077
SPARK_MASTER_HOST=localhost
#Allocation Parameters
SPARK_WORKER_CORES=1
SPARK_WORKER_MEMORY=2G
SPARK_DRIVER_MEMORY=128m
SPARK_EXECUTOR_MEMORY=256m
DEFAULT_MAX_TO_STRING_FIELDS=100
