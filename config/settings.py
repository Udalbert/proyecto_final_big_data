import os
import pymysql
from mongoengine import connect
from mongoengine import disconnect
import mysql.connector
from sqlalchemy import create_engine
from littlenv import littlenv
from pymongo import MongoClient

littlenv.load()

API_VERSION = "v0.1"

CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL")
CELERY_RESULT_BACKEND = os.getenv("CELERY_RESULT_BACKEND")
LIMIT_CONSULT = 300000
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
INSERT_DB = 'append'
# INSERT_DB = 'replace'
DEBUG = os.getenv("DEBUG", False)


_MONGO_URI_WORKSPACE = os.environ.get("_MONGO_URI_WORKSPACE", None)
NAME_DB_WORKSPACE = os.environ.get("NAME_DB_WORKSPACE", "sg_workspace")


def start_connection_workspace():
    return MongoClient(
        _MONGO_URI_WORKSPACE
    )


def connect_db():
    """

    :return: connect mongodb
    """
    connect(
        host=os.environ.get(
            'MONGO_URI',
            'test'
        ),
        alias=os.environ.get('MONGO_NAME', 'test'),
    )


def close_db():
    disconnect(
        alias=os.environ.get('MONGO_NAME', 'test')
    )


def get_engine_sql(
        user: str,
        password: str,
        host: str,
        name: str,
        table: bool = True
):
    url = 'mysql+pymysql://{}:{}@{}:3306/{}'.format(
        os.environ.get(user, 'test'),
        os.environ.get(password, 'test'),
        os.environ.get(host, 'test'),
        os.environ.get(name, 'test') if table else name,
    )
    connect_args = {
        'init_command': "SET @@collation_connection='latin1_spanish_ci'"}
    engine = create_engine(url, connect_args=connect_args, echo=False)
    return engine


def engine_corebanking():
    return get_engine_sql(
        'USER_MYSQL_COREBANKING',
        'PASSWORD_MYSQL_COREBANKING',
        'HOST_MYSQL_COREBANKING',
        'NAME_MYSQL_COREBANKING'
    )


def engine_datamart():
    return get_engine_sql(
        'USER_MYSQL_DATAMART',
        'PASSWORD_MYSQL_DATAMART',
        'HOST_MYSQL_DATAMART',
        'NAME_MYSQL_DATAMART'
    )
