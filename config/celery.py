import os
from celery import Celery
from celery.schedules import crontab
from config.settings import connect_db
from app.core.process import CronProcess, SourcesGlobals


celery_app = Celery(
    'tasks',
    broker=os.getenv("CELERY_BROKER_URL"),
    include=['app.core.tasks']
)


@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Tareas de mantenimiento
    # sender.add_periodic_task(
    #     crontab(minute=30, hour="0"),
    #     task_sources_cron.s(),
    # )
    # sender.add_periodic_task(
    #     crontab(minute=0, hour="2"),
    #     task_sources_cron.s(),
    # )

    # Tareas para correr etl's
    sender.add_periodic_task(
        crontab(minute=0, hour="1"),
        task_cron.s(),
    )
    sender.add_periodic_task(
        crontab(minute=0, hour="3"),
        task_cron.s(),
    )
    # sender.add_periodic_task(
    #     # crontab(hour=1, minute=0, day_of_week=1),
    #     60.0,
    #     task_cron.s(),
    # )


celery_app.conf.timezone = 'America/Bogota'
celery_app.conf.enable_utc = True


@celery_app.task(bind=True)
def task_cron(self):
    connect_db()
    cron_process = CronProcess()
    cron_process.process_cron()


# @celery_app.task(bind=True)
# def task_sources_cron(self):
#     SourcesGlobals.delete_globals()


@celery_app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
