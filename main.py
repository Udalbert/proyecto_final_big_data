from fastapi import FastAPI
from config import routers, settings
from config.settings import API_VERSION
from littlenv import littlenv
littlenv.load()

app = FastAPI(
    title="components-flow",
    description="Etls, contabilidad",
    version=API_VERSION,
    redoc_url="/documentation",
)

app.include_router(
    routers.urls
)

app.add_event_handler(
    "startup",
    settings.connect_db
)

app.add_event_handler(
    "shutdown",
    settings.close_db
)
