# Proyecto final Big Data



<!--
## Table of Contents
* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [API](#api)
* [System errors](#system-errors)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)
-->
<!-- ABOUT THE PROJECT -->
## About The Project

This project is a basic microservice for Etl's in process batch of data,
with the implementation of spark, mongo and sql technologies

### Built With
* Language: Python3
* Framework: FastApi
* Database: MongoDB
* Database: SQL
* Services: Celery 
* Motor: Spark 
* container: Docker 


<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites
It is necessary to have installed MongoDB, bellow is the link with the installation guide from the official page

* [MongoDB Installation](https://docs.mongodb.com/manual/installation/)

Once you have MongoDB installed, and if you are going to run the project on local, please check that mongo
is running in your pc with the next command:

```sh
sudo systemctl status mongod
```

* [Spark](https://spark.apache.org/docs/latest/api/python/getting_started/install.html)

### Installation 

1. Install Virtualenv and activate it
```sh
$ virtualenv -p python3 venv
```
```sh
$ source venv/bin/activate
```
2. Install requirements.txt
```sh
$ pip3 install -r requirements.txt
```
### Execution

### cd docker folder
```sh
$ cd docker/docker_local
```
### build docker
```sh
$ sudo docker-compose build
```
### run docker
```sh
$ sudo docker-compose up
```


## Running whit docker-compose for different envs

* Export your enviroment as environ variable
    ```shell
          export ENV=production
    ```

* Run your docker compose with this command
    ```shell
        docker-compose --file docker/${ENV}/docker-compose.yml up  --build
    ```
    * **Note** if build is not required you can ommit the `--build`
    

## Running whit docker

* Export your enviroment as environ variable
    ```shell
          docker build --no-cache -t proyecto_final:test -f docker/staging/gunicorn/Dockerfile ./
    ```

* Run your docker compose with this command
    ```shell
        ddocker run -p 8001:8001 --rm -it -w="/www" -v $(pwd):/www proyecto_final:test bash
    ```
    
<!-- API -->
## API
The api consists in three endpoints, this will be describe bellow:


<!-- ARCHITECTURE -->
## Architecture


<!-- CONTACT -->
## Contact
Angel Udalber Rodriguez - udgottschalk@gmail.com
